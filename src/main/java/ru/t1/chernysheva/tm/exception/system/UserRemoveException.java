package ru.t1.chernysheva.tm.exception.system;

import ru.t1.chernysheva.tm.exception.system.AbstractSystemException;

public class UserRemoveException extends AbstractSystemException {

    public UserRemoveException() {
        super("Error! You can't remove current user...");
    }

}
