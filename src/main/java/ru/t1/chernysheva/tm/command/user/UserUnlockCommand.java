package ru.t1.chernysheva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-unlock";

    @NotNull
    private final String DESCRIPTION = "Unlock user.";

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
