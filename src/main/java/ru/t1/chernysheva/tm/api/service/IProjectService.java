package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    void changeProjectStatusById (@Nullable String userId, @Nullable String Id, @Nullable Status status);

    void changeProjectStatusByIndex (@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name);

    @Nullable
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

}
